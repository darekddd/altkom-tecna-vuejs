import "bootstrap/dist/css/bootstrap.css";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}


export const decorators = [
  (story) => {
    return ({
      components: { story },
      // <MemoryRouter> <story> ...
      template: /* html */`<div style="margin: 5em;">
          <story/>
        </div>` })
  }
]

