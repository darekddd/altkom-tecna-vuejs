// eslint-disable-next-line
/// <reference path="./vuex.d.ts" />

// import './vuex.d'

import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import { tryLogin } from "./services/Auth";
import store from "./store";

tryLogin();

// Vue.use(router)

(window as any).app = createApp(App)
  .use(store)
  .use(router)
  .mount("#app");
