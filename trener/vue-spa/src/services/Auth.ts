import axios from "axios";

let token = ''

const client_id = 'f65301b39a6e4e1ba152c3123e430fc2';
const redirect_uri = window.origin + '/callback';
const url = 'https://accounts.spotify.com/authorize';



axios.defaults.baseURL = 'https://api.spotify.com/v1/'
// axios.defaults.headers.common = { Authorization: 'Bearer ' + getToken() }

axios.interceptors.request.use(config => {
    config.headers = { ...config.headers, Authorization: 'Bearer ' + getToken() }
    return config
})

axios.interceptors.response.use(ok => ok, err => {
    if (!axios.isAxiosError(err)) throw Error('Invalid response')
    assertSportifyErrorResponse(err.response?.data)

    if (err.response.status === 401) {
        login(); throw new Error('Session expired. Loggin out')
    }

    throw Error(err.response?.data.error.message)
})

function assertSportifyErrorResponse(data: any): asserts data is SportifyErrorResponse {
    // Schema = zod.shape({error: zod.Shape({message:zod.string()})})
    // zod.Infer<Schema>

    if (!('error' in data
        && 'message' in data.error
        && typeof data.error.message === 'string'))
        throw Error('Invalid response')
}

interface SportifyErrorResponse {
    error: {
        message: string, status: number
    }
}


export function login() {

    // const state = new Int8Array(2)
    // crypto.getRandomValues(state);
    const state = window.location.href.replace(window.location.origin, '')
    // localStorage.setItem('XSRF_TOKEN', state.toString());

    const scope = ''

    const redirect = url + '?' + new URLSearchParams({
        response_type: 'token',
        // client_id: client_id,
        client_id,
        scope,
        redirect_uri,
        state: state.toString(),
        show_dialog: 'true'
    })

    window.location.href = (redirect);
    // popup =  window.open(redirect);
    // setInterval( ... popup.window.url.match(access_token)
}

export function getToken() {
    return token
}


export function tryLogin() {

    if (window.location.hash) {
        const { access_token, state } = Object.fromEntries(new URLSearchParams(window.location.hash.replace(/^#\/?/, '')).entries())
        token = access_token
        token && localStorage.setItem('token', JSON.stringify(token))
        if(state){
            window.location.href = window.location.origin + state
        }
    }

    if (!token) {
        token = JSON.parse(localStorage.getItem('token') || 'null')
    }

    if (!token) {
        login()
    }
}