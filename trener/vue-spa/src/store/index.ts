import { createStore } from 'vuex'
import { playlists } from './modules/playlists'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    playlists
  },

})
