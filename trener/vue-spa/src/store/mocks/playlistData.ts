import { Playlist } from "@/components/playlists/Playlist";

export const playlistData: Playlist[] = [
    {
        id: "123",
        name: "Playlist 123",
        public: false,
        description: "Best playlist <b>123</b>",
    },
    {
        id: "234",
        name: "Playlist 234",
        public: true,
        description: "Best playlist <b>234</b>",
    },
    {
        id: "345",
        name: "Playlist 345",
        public: false,
        description: "Best playlist <b>345</b>",
    },
];