import { RootState } from './store/RootState';
// vuex.d.ts
import { Store } from 'vuex'
import store from './store'
import { PlaylistsState } from './store/modules/playlists/PlaylistsState';

declare module '@vue/runtime-core' {
    // declare your own store states
    interface State extends RootState{
        playlists: PlaylistsState
    }

    // provide typings for `this.$store`
    interface ComponentCustomProperties {
        // $store: typeof store
        $store: Store<State>
    }
}